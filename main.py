import sys
import signal
import pygame
import random
import tkinter as tk
from classes import Snake, Cube
from tkinter import messagebox


def handler(sig, frame):
    print('\n\n[!] Exiting...')
    sys.exit(0)


signal.signal(signal.SIGINT, handler)


def draw_grid(width, rows, surface):
    size_btwn = width // rows

    x = 0
    y = 0

    for l in range(rows):
        x += size_btwn
        y += size_btwn

        pygame.draw.line(surface, (255, 255, 255), (x, 0), (x, width))
        pygame.draw.line(surface, (255, 255, 255), (0, y), (width, y))


def redraw_window(surface):
    global width, rows, s, snack
    surface.fill((0, 0, 0))
    s.draw(surface)
    snack.draw(surface)
    draw_grid(width, rows, surface)
    pygame.display.update()


def random_snack(items):
    global rows
    positions = items.body

    while True:
        x = random.randrange(rows)
        y = random.randrange(rows)

        if len(list(filter(lambda z: z.pos == (x, y), positions))) > 0:
            continue

        else:
            break

    return (x, y)


def message_box(subject, content):
    root = tk.Tk()
    root.attributes("-topmost", True)
    root.withdraw()
    messagebox.showinfo(subject, content)

    try:
        root.destroy()

    except:
        pass


def main():
    global width, rows, s, snack
    width = 675
    height = 675
    rows = 27
    win = pygame.display.set_mode((width, height))
    s = Snake((255, 0, 0), (10, 10))
    snack = Cube(random_snack(s), color=(0, 255, 0))
    flag = True
    clock = pygame.time.Clock()

    while flag:
        pygame.time.delay(50)
        clock.tick(10)

        s.move()

        if s.body[0].pos == snack.pos:
            s.add_cube()
            snack = Cube(random_snack(s), color=(0, 255, 0))

        for x in range(len(s.body)):
            if s.body[x].pos in list(map(lambda z: z.pos, s.body[x + 1:])):
                print('Score: ', len(s.body))
                message_box('You Lost!', 'Play again...')
                s.reset((10, 10))
                break

        redraw_window(win)


if __name__ == '__main__':
    main()
